#!/usr/bin/env python3
import os,sys,subprocess,logging,random

e_d=True
try:
	import enable_debug
except ImportError:
	e_d=False
if e_d == True:
	os.system('tput setaf 1')
	logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
else:
	logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def scrclr():
	try:
		screen_clear = config.screen_clear
	except NameError:
		screen_clear = True
	except AttributeError:
		screen_clear = True
	try:
		super_clr = config.super_clr
	except NameError:
		super_clr = False
	except AttributeError:
		super_clr = False
	if screen_clear == True:
		os.system('clear')
	if super_clr == True:
		os.system('clear')
		print('S')
		os.system('clear')
		print('SC')
		os.system('clear')
		print('SCR')
		os.system('clear')
		print('SCRE')
		os.system('clear')
		print('SCREE')
		os.system('clear')
		print('SCREEN')
		os.system('clear')
		print('SCREEN C')
		os.system('clear')
		print('SCREEN CL')
		os.system('clear')
		print('SCREEN CLE')
		os.system('clear')
		print('SCREEN CLEA')
		os.system('clear')
		print('SCREEN CLEAR')
		os.system('clear')
		print('THIS IS')
		os.system('clear')
		print('STUPID')
		os.system('clear')
		print('SCREEN CLEAR')
		os.system('clear')
		print('SCREEN CLEA')
		os.system('clear')
		print('SCREEN CLE')
		os.system('clear')
		print('SCREEN CL')
		os.system('clear')
		print('SCREEN C')
		os.system('clear')
		print('SCREEN')
		os.system('clear')
		print('SCREE')
		os.system('clear')
		print('SCRE')
		os.system('clear')
		print('SCR')
		os.system('clear')
		print('SC')
		os.system('clear')
		print('S')
		os.system('clear')
def killself():
	scrclr()
	print('I (probably) ain\'t programming more functionility.')
	sys.exit(1)
def genkey():
	scrclr()
	os.system('gpg --full-gen-key')
	askcontinue()
	start()
def askexit():
	os.system('rm ' + config.file_used_path + ' ' + config.file_used_path + '.0 -f')
	input('Return to exit. ')
	scrclr()
	sys.exit(0)
def askcontinue():
	os.system('rm ' + config.file_used_path + ' ' + config.file_used_path + '.0 -f')
	input('Return to continue. ')
def add_sign_cmd(runcmd_0):
	if config.sign_opt == 'always':
		DISTURBING_A_PROGRAMMER_WHILE_HES_CODING_IS_NOT_COOL=runcmd_0 + ' --sign'
	elif config.sign_opt == 'never':
		DISTURBING_A_PROGRAMMER_WHILE_HES_CODING_IS_NOT_COOL=runcmd_0
	else:
		DISTURBING_A_PROGRAMMER_WHILE_HES_CODING_IS_NOT_COOL=add_sign_cmd_ask(runcmd_0)
	return DISTURBING_A_PROGRAMMER_WHILE_HES_CODING_IS_NOT_COOL
def add_sign_cmd_ask(runcmd_0):
	scrclr()
	print('Sign the message/file?')
	print('')
	print('0. Yes')
	print('1. No')
	ARE_WE_CONNECTED=input('> ')
	scrclr()
	if ARE_WE_CONNECTED == '0':
		ARE_YOU_THERE=runcmd_0 + ' --sign'
	elif ARE_WE_CONNECTED == '1':
		ARE_YOU_THERE=runcmd_0
	else:
		print("You've inputted an incorrect value, assuming yes.")
		print('')
		askcontinue()
		ARE_YOU_THERE=runcmd_0 + ' --sign'
		scrclr()
	return ARE_YOU_THERE
def showkeys():
	scrclr()
	p=''
	if config.disable_search == False:
		print('What to search for (leave empty for all)')
		p=input('> ')
		scrclr()
	os.system('gpg --list-keys --keyid-format SHORT ' + p)
	print('')
	askcontinue()
	start()
def air():
	return 'NOW'
def edit_used_c_env():
	if config.editor_override == False:
		try:
			edit_used=os.environ['EDITOR']
		except KeyError:
			try:
				edit_used=os.environ['VISUAL']
			except KeyError:
				try:
					edit_used=os.environ['SUDO_EDITOR']
				except:
					edit_used=config.editor
	else:
		edit_used=config.editor
	return edit_used
def edit_used_c():
	if os.system('which sensible-editor > ' + config.file_used_path + '.editchk') == 0:
		try:
			edit_used_chk=open(config.file_used_path + '.editchk', 'r')
			edit_used_stuf=True
		except FileNotFoundError:
			edit_used=edit_used_c_env()
			edit_used_stuf=False
		if edit_used_stuf == True:
			if 'editor' in str(edit_used_chk.readlines()):
				edit_used=subprocess.run(['which', 'sensible-editor'], stdout=subprocess.PIPE).stdout.decode()[:-1]
				if e_d == True:
					os.system('notify-send ' + edit_used)
			else:
				edit_used=edit_used_c_env()
			edit_used_chk.close()
	else:
		edit_used=edit_used_c_env()
	edit_used=edit_used + ' '
	return edit_used
def removekey():
	scrclr()
	print('Choose an option')
	print('')
	print('0. Remove Public Key')
	print('1. Remove Private Key')
	brokenkek0=input('> ')
	scrclr()
	print('Enter the email address of the key you\'d like to remove.')
	if config.list_keys == True:
		if brokenkek0 == '0':
			os.system('gpg --list-keys')
		else:
			os.system('gpg -K')
	if brokenkek0 != '0' and brokenkek0 != '1':
		killself()
	brokenkek1=input('> ')
	if brokenkek1 == '':
		killself()
	scrclr()
	if brokenkek0 == '0':
		os.system('gpg --delete-key ' + brokenkek1)
	else:
		os.system('gpg --delete-secret-key ' + brokenkek1)
	askcontinue()
	start()
def exportkey():
	scrclr()
	print('Choose an option')
	print('')
	print('0. Export Public Key')
	print('1. Export Private Key')
	exportkey_0=input('> ')
	scrclr()
	print('Enter the email address of the key you\'d like to export.')
	if config.list_keys == True:
		if exportkey_0 == '0':
			os.system('gpg --list-keys')
		else:
			os.system('gpg -K')
	if exportkey_0 != '0' and exportkey_0 != '1':
		killself()
	exportkey_1=input('> ')
	if exportkey_1 == '':
		killself()
	scrclr()
	print('What\'s the file name used?')
	exportkey_2=input('> ')
	if exportkey_2 == '':
		killself()
	if exportkey_0 == '0':
		os.system('gpg --armor --export ' + exportkey_1 + '>' + exportkey_2)
	else:
		os.system('gpg --armor --export-secret-keys ' + exportkey_1 + '>' + exportkey_2)
	askcontinue()
	start()
def importkeys():
	scrclr()
	print('Key to import [from ./]')
	if config.list_files == True:
		os.system('ls -l')
	choice_ripilosttrack_lol=input('> ')
	if choice_ripilosttrack_lol == '':
		killself()
	scrclr()
	os.system('gpg --import ' + choice_ripilosttrack_lol)
	askcontinue()
	start()
def editkey():
	scrclr()
	print('Enter the email of the key to edit')
	if config.list_keys == True:
		os.system('gpg --list-keys')
	choice_ripilosttrack_lol=input('> ')
	if choice_ripilosttrack_lol == '':
		killself()
	scrclr()
	os.system('gpg --edit-key ' + choice_ripilosttrack_lol)
	askcontinue()
	start()
def showprivatekeys():
	scrclr()
	p=''
	if config.disable_search == False:
		print('What to search for (leave empty for all)')
		p=input('> ')
		scrclr()
	os.system('gpg -K --keyid-format short ' + p)
	print('')
	askcontinue()
	start()
def decrypt_plaintext():
	scrclr()
	print('Get PGP message from clipboard or editor?')
	print('0. Clipboard')
	print('1. Editor')
	print('')
	de_pgpget=input('> ')
	if de_pgpget == '0':
		os.system('touch ' + config.file_used_path)
		file_used=open(config.file_used_path, 'w')
		file_used.write(subprocess.run(['xclip', '-o', '-selection', 'clipboard'],stdout=subprocess.PIPE).stdout.decode())
		file_used.close()
	elif de_pgpget == '1':
		os.system(edit_used_c() + config.file_used_path)
	scrclr()
	os.system("sed -i 's/^\[.*//' " + config.file_used_path)
	os.system("sed -i 's/—/--/g' " + config.file_used_path)
	os.system('cat ' + config.file_used_path + '|xclip -i -selection clipboard')
	os.system('rm -f ' + config.file_used_path)
	os.system('xclip -o -selection clipboard|gpg -d')
	print('')
	askcontinue()
	start()
def encrypt_plaintext():
	scrclr()
	print('Type what to encrypt.')
	askcontinue()
	scrclr()
	encrypt_plaintext_stuf=edit_used_c()
	if e_d == True:
		os.system('notify-send ' + '"' + encrypt_plaintext_stuf + '"')
	encrypt_plaintext_stuf+=config.file_used_path
	if e_d == True:
		os.system('notify-send ' + '"' + encrypt_plaintext_stuf + '"')
	encrypt_plaintext_stuf+='.txt'
	if e_d == True:
		os.system('notify-send ' + '"' + encrypt_plaintext_stuf + '"')
	os.system(encrypt_plaintext_stuf)
	scrclr()
	if config.add_self_as_r == True:
		print('Who will be the receivers (there will always be ' + config.usr_email + ')?')
	else:
		print('Who will be the receiver(s)?')
	print('Seperate receivers with \' \' (one space)')
	if config.add_self_as_r == True:
		print('Leave empty for ' + config.usr_email + ' only.')
	if config.list_keys == True:
		print('')
		os.system('gpg --list-keys')
	choice_en_1=input('> ')
	if config.add_self_as_r == False:
		if choice_en_1 == '':
			killself()
	if config.add_self_as_r == True:
		runcmd_0='cat ' + config.file_used_path + '.txt|gpg --encrypt --armor -r ' + config.usr_email
	else:
		runcmd_0='cat ' + config.file_used_path + '.txt|gpg --encrypt --armor'
	if choice_en_1 == '':
		runcmd=runcmd_0
	else:
		os.system('touch ' + config.file_used_path)
		file_used=open(config.file_used_path, 'w')
		file_used.write(choice_en_1)
		file_used.close()
		os.system("sed -i 's/ / -r /g' " + config.file_used_path)
		choice_en_1_0=subprocess.run(['cat', config.file_used_path],stdout=subprocess.PIPE).stdout.decode()
		runcmd=runcmd_0 + ' -r ' + choice_en_1_0
	print('')
	scrclr()
	runcmd=runcmd + '>' + config.file_used_path + '.0'
	runcmd=add_sign_cmd(runcmd)
	logging.debug(runcmd)
	os.system(runcmd)
	os.system('cat ' + config.file_used_path + '.0')
	os.system('cat ' + config.file_used_path + '.0|xclip -i -selection clipboard')
	os.system('rm -f ' + config.file_used_path + ' ' + config.file_used_path + '.0 ' + config.file_used_path + '.txt')
	print('')
	print('The PGP message is also copied to XA_CLIPBOARD')
	askcontinue()
	start()
def encrypt_file():
	scrclr()
	print('File to encrypt [from ./]')
	if config.list_files == True:
		os.system('ls -l')
	choice_en_0=input('> ')
	if choice_en_0 == '':
		killself()
	scrclr()
	if config.add_self_as_r == True:
		print('Who will be the receivers (there will always be ' + config.usr_email + ')?')
	else:
		print('Who will be the receiver(s)?')
	print('Seperate receivers with \' \' (one space)')
	if config.add_self_as_r == True:
		print('Leave empty for ' + config.usr_email + ' only.')
	if config.list_keys == True:
		print('')
		os.system('gpg --list-keys')
	choice_en_1=input('> ')
	if config.add_self_as_r == False:
		if choice_en_1 == '':
			killself()
	if config.add_self_as_r == True:
		runcmd_0='gpg --encrypt --armor -r ' + config.usr_email
	else:
		runcmd_0='gpg --encrypt --armor'
	runcmd_0=add_sign_cmd(runcmd_0)
	if choice_en_1 == '':
		runcmd=runcmd_0 + ' ' + choice_en_0
	else:
		os.system('touch ' + config.file_used_path)
		file_used=open(config.file_used_path, 'w')
		file_used.write(choice_en_1)
		file_used.close()
		os.system("sed -i 's/ / -r /g' " + config.file_used_path)
		choice_en_1_0=subprocess.run(['cat', config.file_used_path],stdout=subprocess.PIPE).stdout.decode()
		os.system('rm -f ' + config.file_used_path)
		runcmd=runcmd_0 + ' -r ' + choice_en_1_0 + ' ' + choice_en_0
	scrclr()
	os.system(runcmd)
	scrclr()
	print(choice_en_0 + '.asc has been created')
	print('')
	askcontinue()
	start()
def decrypt_file():
	scrclr()
	print('File to decrypt [from ./]')
	if config.list_files == True:
		os.system('ls -l')
	choice_de_0=input('> ')
	if choice_de_0 == '':
		killself()
	scrclr()
	os.system('touch ' + config.file_used_path)
	file_used=open(config.file_used_path, 'w')
	file_used.write(choice_de_0)
	file_used.close()
	os.system("sed -i 's/\.asc$//' " + config.file_used_path)
	choice_de_0_0=subprocess.run(['cat', config.file_used_path],stdout=subprocess.PIPE).stdout.decode()
	os.system('rm -f ' + config.file_used_path)
	logging.debug(choice_de_0_0)
	runcmd='gpg -d ' + choice_de_0 + '>' + config.file_used_path
	os.system(runcmd)
	scrclr()
	print('Do you want to view the result of ' + choice_de_0_0 + '?')
	print('0. Yes')
	print('1. No')
	print('')
	choice_de_umm=input('> ')
	if choice_de_umm == '0':
		scrclr()
		os.system('cat ' + config.file_used_path)
		askcontinue()
	elif choice_de_umm == '1':
		air()
	else:
		os.system('rm -f' + config.file_used_path)
		killself()
	os.system('rm -f ' + config.file_used_path)
	scrclr()
	print('Do you want to pipe the result to ' + choice_de_0_0 + '?')
	print('0. Yes')
	print('1. No')
	print('')
	choice_de_1=input('> ')
	if choice_de_1 == '0':
		os.system(runcmd + '>' + choice_de_0_0)
	elif choice_de_1 == '1':
		air()
	else:
		killself()
	askcontinue()
	start()
def old_edit_file(file_edit):
	os.system('mkdir ' + config.file_used_path + '.f')
	os.system('cp ' + file_edit + ' ' + config.file_used_path + '.f')
	logging.debug(os.environ['PWD'])
	os.system('gpg -d ' + file_edit + '>' + config.file_used_path)
	scrclr()
	os.system(edit_used_c() + config.file_used_path)
	os.system('mv ' + config.file_used_path + ' ' + config.file_used_path + '.f/' + file_edit)
	scrclr()
	print('Well... I guess you have to re-enter the recipients because I\'ll program auto checks never.')
	print('')
	if config.add_self_as_r == True:
		print('Who will be the receivers (there will always be ' + config.usr_email + ')?')
	else:
		print('Who will be the receiver(s)?')
	print('Seperate receivers with \' \' (one space)')
	if config.add_self_as_r == True:
		print('Leave empty for ' + config.usr_email + ' only.')
	if config.list_keys == True:
		print('')
		os.system('gpg --list-keys')
	choice_en_1=input('> ')
	if config.add_self_as_r == False:
		if choice_en_1 == '':
			killself()
	if config.add_self_as_r == True:
		runcmd_0='gpg --encrypt --armor -r ' + config.usr_email
	else:
		runcmd_0='gpg --encrypt --armor'
	choice_en_0 = config.file_used_path + '.f/' + file_edit
	if choice_en_1 == '':
		runcmd=add_sign_cmd(runcmd_0) + ' ' + choice_en_0
	else:
		os.system('touch ' + config.file_used_path)
		file_used=open(config.file_used_path, 'w')
		file_used.write(choice_en_1)
		file_used.close()
		os.system("sed -i 's/ / -r /g' " + config.file_used_path)
		choice_en_1_0=subprocess.run(['cat', config.file_used_path],stdout=subprocess.PIPE).stdout.decode()
		os.system('rm -f ' + config.file_used_path)
		runcmd=runcmd_0 + ' -r ' + choice_en_1_0
		runcmd=add_sign_cmd(runcmd)
		runcmd+=' ' + choice_en_0
	if e_d == True:
		os.system("rofi -e '" + runcmd + "'")
	os.system(runcmd)
	logging.debug(os.environ['PWD'])
	logging.debug(file_edit)
	os.system('mv ' + config.file_used_path + '.f/' + file_edit + '.asc ./' + file_edit)
	os.system('rm -rf ' + config.file_used_path + '.f ' + config.file_used_path)
def ask_edit_file(Y_U_256BYTE_HEX_STRING):
	if Y_U_256BYTE_HEX_STRING == True:
		print('Your config isn\'t right')
		print('')
	print('What to use?')
	print('')
	print('0. vigpg (No armor support)')
	print('1. Old Thingy (armor support)')
	print('')
	f8cf5e332cb60b9176ee4884cc15fc822c7f61f5=input('> ')
	if f8cf5e332cb60b9176ee4884cc15fc822c7f61f5 != '0' and f8cf5e332cb60b9176ee4884cc15fc822c7f61f5 != '1':
		start()
	scrclr()
	return f8cf5e332cb60b9176ee4884cc15fc822c7f61f5
def edit_file():
	scrclr()
	if config.edit_file_which == 'vigpg':
		f8cf5e332cb60b9176ee4884cc15fc822c7f61f5='0'
	elif config.edit_file_which == 'old':
		f8cf5e332cb60b9176ee4884cc15fc822c7f61f5='1'
	elif config.edit_file_which == 'ask':
		f8cf5e332cb60b9176ee4884cc15fc822c7f61f5=ask_edit_file(False)
	else:
		f8cf5e332cb60b9176ee4884cc15fc822c7f61f5=ask_edit_file(True)
	print('File to edit [from ./]')
	if config.list_files == True:
		os.system('ls -l')
	file_edit=input('> ')
	if file_edit == '':
		killself()
	scrclr()
	if f8cf5e332cb60b9176ee4884cc15fc822c7f61f5 == '0':
		os.system('vigpg ' + file_edit)
	else:
		old_edit_file(file_edit)
	scrclr()
	print(file_edit + ' has been edited.')
	print('')
	askcontinue()
	start()
def get_revoke():
	scrclr()
	print('Which key to generate a revocation certificate of?')
	print('ONE AT A TIME AND USE KEYID!')
	print('')
	if config.list_keys == True:
		os.system('gpg -K --keyid-format SHORT')
	to_revoke=input('> ')
	if to_revoke == '':
		killself()
	scrclr()
	to_save=input('Where to save it? ')
	if to_save == '':
		killself()
	scrclr()
	os.system('gpg --gen-revoke ' + to_revoke + ' > ' + to_save)
	scrclr()
	print('Revocation certificate of ' + to_revoke + ' has been created at ' + to_save)
	print('')
	askcontinue()
	start()
def wat_keyserver():
	if config.keyserver == '':
		keyserver_used=input('Enter the keyserver you\'d like to use: ')
		if keyserver_used == '':
			start()
		scrclr()
	else:
		keyserver_used=config.keyserver
	return keyserver_used
def recv_key():
	scrclr()
	keysrv=wat_keyserver()
	logging.debug(keysrv)
	torun='gpg --keyserver ' + keysrv + ' --refresh-key'
	logging.debug(torun)
	os.system(torun)
	print('')
	askcontinue()
	start()
def upload_key():
	scrclr()
	keyserver_used=wat_keyserver()
	print('Enter your key ID/email')
	if config.list_keys == True:
		os.system('gpg -k --keyid-format SHORT')
	print('')
	logging.debug(keyserver_used)
	TWIT_GOT_MY_NUMBER=input('> ')
	if TWIT_GOT_MY_NUMBER == '':
		start()
	scrclr()
	print('Are you sure you want to upload key `' + TWIT_GOT_MY_NUMBER + '`?')
	print('')
	print('0. No')
	print('1. Yes')
	print('')
	TWIT_WHY=input('> ')
	if TWIT_WHY != '1':
		start()
	scrclr()
	print('Are you **REALLY** sure you want to upload key `' + TWIT_GOT_MY_NUMBER + '`?')
	TWIT_ANSWER_ME=str(random.randint(0, 12513872650))
	print('Type the number ' + TWIT_ANSWER_ME)
	print('')
	HACK_NUMBER_LEAKED=input('> ')
	if HACK_NUMBER_LEAKED != TWIT_ANSWER_ME:
		start()
	scrclr()
	print('Are you **REALLY** **REALLY** sure you want to upload key `' + TWIT_GOT_MY_NUMBER + '`?')
	ADALARAN_NO_LONGER_LOVES_ME='Yes, I am 1000% sure to upload key ' + TWIT_GOT_MY_NUMBER + '.'
	print('Type `' + ADALARAN_NO_LONGER_LOVES_ME + '`')
	print('')
	if input('> ') != ADALARAN_NO_LONGER_LOVES_ME:
		start()
	scrclr()
	os.system('gpg --keyserver ' + keyserver_used + ' --send-keys ' + TWIT_GOT_MY_NUMBER)
	if e_d == False:
		scrclr()
	print('Done uploading key ' + TWIT_GOT_MY_NUMBER)
	print('If this is an accident, how did you upload the wrong key that many times?')
	print('')
	askcontinue()
	start()
def start():
	scrclr()
	logging.debug('!!!!!!!!!!!!!!!!!!')
	logging.debug('DEBUG MODE ENABLED')
	logging.debug('!!!!!!!!!!!!!!!!!!')
	print('Welcome to GPGp (made by blank X for blank X (and probably other people))')
	print('')
	if importc_error == True:
		print('Missing config, have you tried copying sample.config.py as config.py and editing it?')
		print('You should exit GPGp now.')
		print('')
	if checkvar_error == True:
		print('A variable in config.py is missing.')
		print('You should exit GPGp now.')
		print('')
	print('Choose an option:')
	print('x. Exit')
	print('0. Decrypt')
	print('1. Encrypt')
	print('2. Edit .asc file')
	print('')
	print('3. List keys')
	print('4. List private keys')
	print('5. Generate key')
	print('')
	print('6. Import key/revocation certificate')
	print('7. Export key')
	print('8. Generate revocation certificate')
	print('')
	print('9. Remove key')
	print('-. Edit key')
	print('')
	print('=. Upload key to keyserver')
	print('q. Refresh keys')
	choice_0=input('> ')
	if choice_0 == '0':
		scrclr()
		print('Choose an option:')
		print('0. Plaintext')
		print('1. File')
		choice_de=input('> ')
		if choice_de == '0':
			decrypt_plaintext()
		elif choice_de == '1':
			decrypt_file()
		else:
			killself()
	elif choice_0 == 'q' or choice_0 == 'Q':
		recv_key()
	elif choice_0 == '=':
		upload_key()
	elif choice_0 == '8':
		get_revoke()
	elif choice_0 == '1':
		scrclr()
		print('Choose an option:')
		print('0. Plaintext')
		print('1. File')
		choice_en=input('> ')
		if choice_en == '0':
			encrypt_plaintext()
		elif choice_en == '1':
			encrypt_file()
		else:
			killself()
	elif choice_0 == '2':
		edit_file()
	elif choice_0 == '3':
		showkeys()
	elif choice_0 == '4':
		showprivatekeys()
	elif choice_0 == '5':
		genkey()
	elif choice_0 == '7':
		exportkey()
	elif choice_0 == '9':
		removekey()
	elif choice_0 == '6':
		importkeys()
	elif choice_0 == 'x' or choice_0 == 'X':
		askexit()
	elif choice_0 == '-':
		editkey()
	else:
		killself()
importc_error = False
checkvar_error = False
try:
	import config
except ImportError:
	importc_error = True
try:
	logging.debug([config.usr_email, config.file_used_path, config.list_keys, config.list_files, config.add_self_as_r, config.editor, config.editor_override, config.screen_clear, config.super_clr, config.sign_opt, config.edit_file_which, config.keyserver, config.disable_search])
except NameError:
	checkvar_error = True
except AttributeError:
	checkvar_error = True
start()
# Hmm... you found this. Congrats, can I be saved?
# This is an easter egg, I guess.
#[H[J[H[2J[3J[H[J[31m#[31m  _ __   ___
#[31m | '_ \ / _ \
#[31m | | | | (_) |
#[31m |_| |_|\___/
