# User's email for PGP encryption
usr_email = 'email@example.com'

# File path used
file_used_path = '/tmp/.gpgp'

# List keys where possible
list_keys = True

# List files where possible
list_files = True

# Add yourself as a recipient while encrypting plaintext/files
add_self_as_r = True

# Editor used
# It'll find sensible-editor, if it doesn't exist then it'll use the variables below
# It'll use ${EDITOR}, ${VISUAL}, ${SUDO_EDITOR} or the editor specified below, which ever comes first
editor = 'nano'

# Force to use editor specified?
editor_override = False

# Enable (excessive) screen clear?
screen_clear = True

# Enable super screen clear?
super_clr = False

# Options when asked for signing:
# always - Always sign, obviously
# ask - Asks if you want to sign
# never - Never sign, do you need these tho?
sign_opt = 'ask'

# Options when asked `Which one to choose` when using `Edit .asc File`
# vigpg - Uses vigpg
# old - Uses old thingy
# ask - Asks you everytime
edit_file_which = 'ask'

# The keyserver used
# Leave empty if you want to be asked everytime
keyserver = ''

# Disable searching for key showing?
disable_search = False
