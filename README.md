# GPGp

A CLI which (probably) is a GPG frontend (or backend, idk). If you have problems, it's written for me, I (probably) won't help you.

## Setup
+ Install `gnupg` and `xclip` using your package manger.
    + Arch Linux: `sudo pacman -S gnupg xclip`
    + Debian/Ubuntu/Linux Mint: `sudo apt install gnupg xclip`
+ Clone this repo (`git clone https://gitlab.com/blankX/gpgp.git`)
+ cd into the repo's directory (`cd gpgp`)
    + Copy `sample.config.py` to `config.py` (`cp sample.config.py config.py`)
    + Edit `config.py` with your text editor

## Start
+ `python3 gpgp.py`
